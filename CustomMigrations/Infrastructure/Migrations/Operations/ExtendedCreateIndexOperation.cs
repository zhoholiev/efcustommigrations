﻿namespace CustomMigrations.Infrastructure.Migrations.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations.Model;
    using System.Linq;
    using Models;

    /// <summary>
    ///     Represents creating an extended database index.
    /// </summary>
    internal class ExtendedCreateIndexOperation : MigrationOperation
    {
        private readonly ICollection<IndexColumnModel> _columns = new List<IndexColumnModel>();
        private readonly ICollection<string> _includes = new List<string>();
        private string _name;
        private string _table;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExtendedCreateIndexOperation" /> class.
        /// </summary>
        /// <param name="anonymousArguments">
        ///     Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
        /// </param>
        public ExtendedCreateIndexOperation(object anonymousArguments = null)
            : base(anonymousArguments)
        {
        }

        /// <summary>
        ///     Gets the columns collection.
        /// </summary>
        /// <value>
        ///     The columns collection.
        /// </value>
        public ICollection<IndexColumnModel> Columns
        {
            get { return _columns; }
        }

        /// <summary>
        ///     Gets the non-key columns to be added to the leaf level of the nonclustered index.
        /// </summary>
        /// <value>
        ///     The the non-key columns to be added to the leaf level of the nonclustered index.
        /// </value>
        public ICollection<string> Includes
        {
            get { return _includes; }
        }

        /// <summary>
        ///     Gets or sets the name of the table.
        /// </summary>
        /// <value>
        ///     The name of the table.
        /// </value>
        /// <exception cref="System.ArgumentException">Table name is null or whitespace.;value</exception>
        public string Table
        {
            get { return _table; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Table name is null or whitespace.", "value");
                }
                _table = value;
            }
        }

        /// <summary>
        ///     Gets or sets the name of the index.
        /// </summary>
        /// <value>
        ///     The name of the index.
        /// </value>
        public string Name
        {
            get
            {
                return _name ??
                       IndexOperation.BuildDefaultName(
                           _columns.Where(c => c != null && !string.IsNullOrWhiteSpace(c.Name)).Select(c => c.Name));
            }
            set { _name = value; }
        }

        /// <summary>
        ///     Gets or sets a value indicating if this is a unique index.
        /// </summary>
        /// <value>
        ///     The value indicating if this is a unique index.
        /// </value>
        public bool IsUnique { get; set; }

        /// <summary>
        ///     Gets or sets whether this is a clustered index.
        /// </summary>
        /// <value>
        ///     Whether this is a clustered index.
        /// </value>
        public bool IsClustered { get; set; }

        /// <summary>
        ///     Gets or sets the WHERE option (&lt;filter_predicate&gt;).
        /// </summary>
        /// <value>
        ///     The WHERE option.
        /// </value>
        public string Where { get; set; }

        /// <summary>
        ///     Gets or sets the WITH option (&lt;relational_index_option&gt; [ ,...n ]).
        /// </summary>
        /// <value>
        ///     The WITH option.
        /// </value>
        public string With { get; set; }

        /// <summary>
        ///     Gets or sets the ON option (partition_scheme_name ( column_name ) | filegroup_name | default).
        /// </summary>
        /// <value>
        ///     The ON option.
        /// </value>
        public string On { get; set; }

        /// <summary>
        ///     Gets or sets the FILESTREAM_ON option (filestream_filegroup_name | partition_scheme_name | "NULL").
        /// </summary>
        /// <value>
        ///     The FILESTREAM_ON option.
        /// </value>
        public string FileStreamOn { get; set; }

        public override bool IsDestructiveChange
        {
            get { return false; }
        }
    }
}