﻿namespace CustomMigrations.Infrastructure.Migrations.Models
{
    using System;

    /// <summary>
    ///     Represents information about an index column.
    /// </summary>
    internal class IndexColumnModel
    {
        /// <summary>
        ///     Gets or sets the name of the column.
        /// </summary>
        /// <value>
        ///     The name of the column.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the sort direction.
        /// </summary>
        /// <value>
        ///     The sort direction.
        /// </value>
        public SortDirection SortDirection { get; set; }

        /// <summary>
        ///     Performs an implicit conversion from <see cref="System.String" /> to <see cref="IndexColumnModel" />.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     The result of the conversion.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">value</exception>
        public static implicit operator IndexColumnModel(string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            return new IndexColumnModel {Name = value, SortDirection = SortDirection.Ascending};
        }
    }
}