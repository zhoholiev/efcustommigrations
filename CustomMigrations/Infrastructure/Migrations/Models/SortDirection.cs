﻿namespace CustomMigrations.Infrastructure.Migrations.Models
{
    /// <summary>
    ///     Index column sort direction.
    /// </summary>
    internal enum SortDirection
    {
        Ascending,
        Descending
    }
}