﻿namespace CustomMigrations.Infrastructure.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.Migrations.Utilities;
    using System.Data.Entity.SqlServer;
    using System.Linq;
    using Models;
    using Operations;

    /// <summary>
    ///     Custom provider to convert provider agnostic migration operations into SQL commands
    ///     that can be run against a Microsoft SQL Server database.
    /// </summary>
    internal sealed class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
    {
        private static readonly IDictionary<SortDirection, string> SortDirectionDescriptionMap = new Dictionary
            <SortDirection, string>
        {
            {SortDirection.Ascending, "ASC"},
            {SortDirection.Descending, "DESC"}
        };

        /// <summary>
        ///     Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.MigrationOperation" />.
        ///     Allows derived providers to handle additional operation types.
        ///     Generated SQL should be added using the Statement method.
        /// </summary>
        /// <param name="migrationOperation">The operation to produce SQL for.</param>
        /// <exception cref="System.ArgumentNullException">migrationOperation</exception>
        protected override void Generate(MigrationOperation migrationOperation)
        {
            if (migrationOperation == null)
            {
                throw new ArgumentNullException("migrationOperation");
            }

            var createIndexOperation = migrationOperation as ExtendedCreateIndexOperation;
            if (createIndexOperation == null)
            {
                return;
            }

            Generate(createIndexOperation);
        }

        /// <summary>
        ///     Generates SQL for a
        ///     <see cref="T:CustomMigrations.Infrastructure.Migrations.Operations.ExtendedCreateIndexOperation" />.
        /// </summary>
        /// <param name="createIndexOperation">The operation to produce SQL for.</param>
        /// <exception cref="System.ArgumentNullException">createIndexOperation</exception>
        private void Generate(ExtendedCreateIndexOperation createIndexOperation)
        {
            if (createIndexOperation == null)
            {
                throw new ArgumentNullException("createIndexOperation");
            }

            using (IndentedTextWriter writer = Writer())
            {
                writer.Write("CREATE ");

                if (createIndexOperation.IsUnique)
                {
                    writer.Write("UNIQUE ");
                }

                if (createIndexOperation.IsClustered)
                {
                    writer.Write("CLUSTERED ");
                }

                writer.Write("INDEX ");
                writer.WriteLine(Quote(createIndexOperation.Name));
                writer.Indent++;
                writer.Write("ON ");
                writer.Write(Name(createIndexOperation.Table));

                writer.Write("(");
                writer.Write(string.Join(", ",
                    createIndexOperation.Columns.Where(c => c != null && !string.IsNullOrWhiteSpace(c.Name))
                        .Select(c => Quote(c.Name) + " " + SortDirectionDescriptionMap[c.SortDirection])));
                writer.Write(")");

                // Skip the INCLUDE part for clustered indexes.
                if (!createIndexOperation.IsClustered && createIndexOperation.Includes.Count > 0)
                {
                    writer.WriteLine();
                    writer.Write("INCLUDE (");
                    writer.Write(string.Join(", ",
                        createIndexOperation.Includes.Where(c => !string.IsNullOrWhiteSpace(c)).Select(Quote)));
                    writer.Write(")");
                }

                if (!string.IsNullOrWhiteSpace(createIndexOperation.Where))
                {
                    writer.WriteLine();
                    writer.Write("WHERE ");
                    writer.Write(createIndexOperation.Where);
                }

                if (!string.IsNullOrWhiteSpace(createIndexOperation.With))
                {
                    writer.WriteLine();
                    writer.Write("WITH (");
                    writer.Write(createIndexOperation.With);
                    writer.Write(")");
                }

                if (!string.IsNullOrWhiteSpace(createIndexOperation.On))
                {
                    writer.WriteLine();
                    writer.Write("ON ");
                    writer.Write(createIndexOperation.On);
                }

                if (!string.IsNullOrWhiteSpace(createIndexOperation.FileStreamOn))
                {
                    writer.WriteLine();
                    writer.Write("FILESTREAM_ON ");
                    writer.Write(createIndexOperation.FileStreamOn);
                }

                Statement(writer);
            }
        }
    }
}