﻿namespace CustomMigrations.Infrastructure.Extensions
{
    using System;
    using Migrations.Models;

    internal static class StringExtensions
    {
        /// <summary>
        ///     Creates the index column model with the ascending sort direction.
        /// </summary>
        /// <param name="value">The column name.</param>
        /// <returns>The index column model.</returns>
        /// <exception cref="System.ArgumentNullException">value</exception>
        public static IndexColumnModel Ascending(this string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            return new IndexColumnModel {Name = value, SortDirection = SortDirection.Ascending};
        }

        /// <summary>
        ///     Creates the index column model with the descending sort direction.
        /// </summary>
        /// <param name="value">The column name.</param>
        /// <returns>The index column model.</returns>
        /// <exception cref="System.ArgumentNullException">value</exception>
        public static IndexColumnModel Descending(this string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            return new IndexColumnModel {Name = value, SortDirection = SortDirection.Descending};
        }
    }
}