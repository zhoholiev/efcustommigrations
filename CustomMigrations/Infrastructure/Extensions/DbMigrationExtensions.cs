﻿namespace CustomMigrations.Infrastructure.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using Migrations.Models;
    using Migrations.Operations;

    internal static class DbMigrationExtensions
    {
        /// <summary>
        ///     Adds an operation to create an table index.
        /// </summary>
        /// <param name="migration">The database migration instance.</param>
        /// <param name="table">
        ///     The name of the table to create the index on. Schema name is optional, if no schema is specified
        ///     then dbo is assumed.
        /// </param>
        /// <param name="columns">The name of the columns to create the index on.</param>
        /// <param name="includes">The includes.</param>
        /// <param name="unique">
        ///     A value indicating if this is a unique index. If no value is supplied a non-unique index will be
        ///     created.
        /// </param>
        /// <param name="name">
        ///     The name to use for the index in the database. If no value is supplied a unique name will be
        ///     generated.
        /// </param>
        /// <param name="clustered">A value indicating whether or not this is a clustered index.</param>
        /// <param name="where">The WHERE option (&lt;filter_predicate&gt;).</param>
        /// <param name="with">The WITH option (&lt;relational_index_option&gt; [ ,...n ]).</param>
        /// <param name="on">The ON option (partition_scheme_name ( column_name ) | filegroup_name | default).</param>
        /// <param name="fileStreamOn">The FILESTREAM_ON option (filestream_filegroup_name | partition_scheme_name | "NULL").</param>
        /// <param name="anonymousArguments">
        ///     Additional arguments that may be processed by providers. Use anonymous type syntax to
        ///     specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        ///     migration
        ///     or
        ///     columns
        /// </exception>
        /// <exception cref="System.ArgumentException">
        ///     Table name is null or whitespace.;table
        ///     or
        ///     Columns collection is empty.;columns
        /// </exception>
        public static void CreateIndex(this DbMigration migration,
            string table,
            ICollection<IndexColumnModel> columns,
            ICollection<string> includes = null,
            bool unique = false,
            string name = null,
            bool clustered = false,
            string where = null,
            string with = null,
            string on = null,
            string fileStreamOn = null,
            object anonymousArguments = null)
        {
            if (migration == null)
            {
                throw new ArgumentNullException("migration");
            }

            if (string.IsNullOrWhiteSpace(table))
            {
                throw new ArgumentException("Table name is null or whitespace.", "table");
            }

            if (columns == null)
            {
                throw new ArgumentNullException("columns");
            }

            if (columns.Count == 0)
            {
                throw new ArgumentException("Columns collection is empty.", "columns");
            }

            var createIndexOperation = new ExtendedCreateIndexOperation(anonymousArguments)
            {
                Table = table,
                IsUnique = unique,
                Name = name,
                IsClustered = clustered,
                Where = where,
                With = with,
                On = on,
                FileStreamOn = fileStreamOn
            };

            foreach (IndexColumnModel column in columns)
            {
                createIndexOperation.Columns.Add(column);
            }

            if (includes != null)
            {
                foreach (string column in includes)
                {
                    createIndexOperation.Includes.Add(column);
                }
            }

            ((IDbMigration) migration).AddOperation(createIndexOperation);
        }
    }
}