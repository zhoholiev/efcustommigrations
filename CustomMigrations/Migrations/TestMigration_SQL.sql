﻿CREATE INDEX [IX_Column1_Column2_Column3]
    ON [dbo].[TestTable]([Column1] DESC, [Column2] ASC, [Column3] ASC)
    INCLUDE ([Column4], [Column5])
    WHERE [Column6] = 'Some filter'
    WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF)
    ON [PRIMARY]
