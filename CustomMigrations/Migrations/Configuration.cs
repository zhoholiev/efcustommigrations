﻿namespace CustomMigrations.Migrations
{
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.SqlServer;
    using Infrastructure.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DbContext>
    {
        public Configuration()
        {
            SetSqlGenerator(SqlProviderServices.ProviderInvariantName, new CustomSqlServerMigrationSqlGenerator());
        }
    }
}