﻿namespace CustomMigrations.Migrations
{
    using System.Data.Entity;
    using System.Data.Entity.SqlServer;
    using Infrastructure.Migrations;

    public class CustomDbConfiguration : DbConfiguration
    {
        public CustomDbConfiguration()
        {
            SetMigrationSqlGenerator(SqlProviderServices.ProviderInvariantName,
                () => new CustomSqlServerMigrationSqlGenerator());
        }
    }
}