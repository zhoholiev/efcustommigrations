﻿namespace CustomMigrations.Migrations
{
    using System.Data.Entity.Migrations;
    using Infrastructure.Extensions;

    public partial class TestMigration : DbMigration
    {
        public override void Up()
        {
            //Sql("CREATE NONCLUSTERED INDEX [IX_Column1_Column2_Column3] ON [dbo].[TestTable]" +
            //    "(" +
            //    "    [Column1] DESC," +
            //    "    [Column2] ASC," +
            //    "    [Column3] ASC" +
            //    ")" +
            //    " INCLUDE (" +
            //    "    [Column4]," +
            //    "    [Column5]" +
            //    ")" +
            //    " WHERE [Column6] = 'Some filter'" +
            //    " WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]");

            this.CreateIndex("dbo.TestTable",
                columns: new[] {"Column1".Descending(), "Column2".Ascending(), "Column3"},
                includes: new[] {"Column4", "Column5"},
                where: "[Column6] = 'Some filter'",
                with: "SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF",
                on: "[PRIMARY]"
                );
        }

        public override void Down()
        {
            DropIndex("dbo.TestTable", new[] {"Column1", "Column2", "Column3"});
        }
    }
}